package com.example.providerimagenes;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    ImageView img;
    Button btn1, btn2;
    Bitmap copia;
    Canvas c;
    float x,y;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img = findViewById(R.id.imageView);
        btn1 = findViewById(R.id.button);
        btn2 = findViewById(R.id.button2);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        img.setOnTouchListener(this);
        c= new Canvas();

        this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 123);

    }
    @Override
    public void onClick(View v) {
        if(v==btn1){

            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            i.setType(MediaStore.Images.Media.CONTENT_TYPE);
            startActivityForResult(i,770);



        }else if(v==btn2){

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==770){
            if(resultCode==RESULT_OK){
                Uri url=data.getData();
                try {
                    Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(),url);
                    img.setImageBitmap(foto);
                    copia = foto.copy(Bitmap.Config.ARGB_8888,true);
                    c= new Canvas(copia);
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(this,"Mal",Toast.LENGTH_SHORT);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction()==MotionEvent.ACTION_BUTTON_PRESS){
            x=event.getX();
            y=event.getY();
        }else if(event.getAction()==MotionEvent.ACTION_MOVE){
            Paint p = new Paint();
            p.setColor(Color.MAGENTA);
            c.drawLine(x,y,event.getX(),event.getY(),p);
            x=event.getX();
            y=event.getY();
            img.setImageBitmap(copia);
        }
        return false;
    }
}
