﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace HilosWindows4
{
    public partial class Form1 : Form
    {
        public delegate void CallbackDelegate();
        public delegate void FuncionDelegate();
        public delegate void ModificaTextBox(string text);
        String[] numeros = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };

        public Form1()
        {
            InitializeComponent();
            button4.Click += async (s, e) =>
            {
                listBox1.DataSource = Pares().ToList();
            };
            button5.Click += async (s, e) =>
            {
                listBox1.DataSource = Impares().ToList();
            };
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Thread ts = new Thread(Proceso);
            FuncionDelegate fd = () =>
            {
                ModificaTextBox f = (p) =>
                {
                    textBox1.Text = p;
                };
                textBox1.BeginInvoke(f, new string[] { "Termino" });
            };
            ts.Start();

        }

        private void Proceso(Object callback)
        {
            ModificaTextBox f = modificar;
            textBox1.BeginInvoke(f,new string[] { "Comenzó" });
            DateTime fin = DateTime.Now.AddSeconds(10);
            while (DateTime.Now <= fin) ;

            //(FuncionDelegate)callback;
        }

        private void callback1()
        {
            
        }

        private void Proceso2()
        {
            ModificaTextBox f = modificar;
            textBox1.BeginInvoke(f,new string[] { "Comenzó" });
            DateTime fin = DateTime.Now.AddSeconds(10);
            while (DateTime.Now <= fin) ;

        }

        private void modificar(string text)
        {
            textBox1.Text = text;
        }

        private string Proceso3()
        {
            ModificaTextBox f = modificar;
            textBox1.BeginInvoke(f, new string[] { "Comenzó" });
            DateTime fin = DateTime.Now.AddSeconds(10);
            while (DateTime.Now <= fin)
            {
                if((fin- DateTime.Now).TotalSeconds >= 1)
                {
                    ModificaTextBox mt = (x) => { textBox2.Text = x; };
                    textBox2.BeginInvoke(mt, DateTime.Now.ToLongTimeString());

                }
            } 

            return "Termino";
        }
        

        private void callback(IAsyncResult ar)
        {
            ModificaTextBox m = (p) =>
            {
                textBox1.Text = p;
            };
            textBox1.BeginInvoke(m,new string[] { "Termino" });
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            FuncionDelegate fd = Proceso2;
            fd.BeginInvoke(callback, null);

        }

        private async void Button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = await Task<string>.Run(Proceso3);
        }

        
        private IEnumerable<String> Pares()
        {
            for(int i = 0; i<5; i++)
            {
                yield return numeros[i * 2];
            }
        }
        private IEnumerable<String> Impares()
        {
            for (int i = 0; i < 5; i++)
            {
                yield return numeros[i * 2+1];
            }
        }
    }
}
