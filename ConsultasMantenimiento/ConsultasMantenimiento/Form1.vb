﻿Public Class Form1

    'Equipos 1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.Memoria <= 64
               Select x.CodEquipo, x.Procesador, x.Velocidad).ToList()

        DataGridView1.DataSource = res
    End Sub

    'Equipos 2
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim resa As String = InputBox("Codigo de Equipo")
        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.CodEquipo = resa
               Select x.CodEquipo, x.Procesador, x.Velocidad).ToList()

        DataGridView1.DataSource = res


    End Sub

    'Equipos 3
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim min As Integer
        Dim max As Integer
        Try
            min = InputBox("Capacidad de disco 1")
            max = InputBox("Capacidad de disco 2")
        Catch ex As Exception
            MsgBox("Faltan datos")

        End Try

        If (min > max) Then
            min = min + max
            max = min - max
            min = min - max
        End If

        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.DiscoDuro > min And x.DiscoDuro < max
               Select x.CodEquipo, x.Procesador, x.DiscoDuro).ToList()

        DataGridView1.DataSource = res

    End Sub

    'Equipos 4
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.Procesador = "PENTIUM III"
               Select x.CodEquipo, x.Procesador, x.DiscoDuro).ToList()

        DataGridView1.DataSource = res
    End Sub

    'Equipos 5
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.Procesador = "PENTIUM" Or x.Procesador = "Pentium II"
               Select x.CodEquipo, x.Procesador, x.DiscoDuro).ToList()

        DataGridView1.DataSource = res
    End Sub

    'Equipos 6
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim res As Object
        res = (From x In modelo.Equipos
               Where x.CDROM < 10 Or x.CDROM > 30
               Select x.CodEquipo, x.Procesador, x.DiscoDuro).ToList()

        DataGridView1.DataSource = res
    End Sub

    'Equipos 7
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim res As Object

        'res = (From x In modelo.Equipos
        '       Order By x.Velocidad Descending
        '       Select x.CodEquipo, x.Procesador, x.DiscoDuro, x.Velocidad).Take(1).ToList()

        res = (From ep In modelo.Equipos
               Aggregate eqaux In modelo.Equipos
                       Into Maximo = Max(eqaux.Velocidad)
               Where ep.Velocidad = Maximo
               Select New With {ep.Velocidad}).ToList()

        DataGridView1.DataSource = res
    End Sub

    'Equipos 8
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim res As Object


        'Encapsulado de un int en tabla

        'Dim res as Integer
        'Dim lista as ArrayList()
        'lista.Add(new With{res}

        res = (From x In modelo.Equipos
               Order By x.Memoria Ascending
               Select x.CodEquipo, x.Procesador, x.Memoria).Take(1).ToList()
        DataGridView1.DataSource = res


    End Sub


    'Usuarios 1

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click

        'Cuidado con esto!!!!

        Dim resp As Integer = InputBox("Codigo de Equipo")
        Dim res As Object
        res = (From x In modelo.Usuarios
               Where x.CodEquipo = resp
               Select x.Nombre, x.Apellido).ToList()
        DataGridView2.DataSource = res

    End Sub

    'Usuarios 2
    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Dim resp As String = InputBox("Codigo de Usuario")
        Dim res As Object
        res = (From x In modelo.Usuarios
               Where x.CodUsuario = resp
               Select x.CodUsuario, x.Nombre, x.Apellido, x.Direccion, x.Telefono).ToList()
        DataGridView2.DataSource = res
    End Sub

    'Usuarios 3
    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim res As Object
        res = (From x In modelo.Usuarios
               Where x.Apellido.StartsWith("a") And x.Nombre.StartsWith("u")
               Select x.CodUsuario, x.Nombre, x.Apellido, x.Direccion, x.Telefono).ToList()
        DataGridView2.DataSource = res
    End Sub

    'Usuarios 4
    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim res As Object
        res = (From x In modelo.Usuarios
               Where x.CodEquipo = "0001" Or x.CodEquipo = "0002"
               Select x.CodUsuario, x.Nombre, x.Apellido, x.Direccion, x.Telefono).ToList()
        DataGridView2.DataSource = res
    End Sub

    'Usuarios 5
    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Dim resp As String = InputBox("Codigo de Usuario")
        Dim res As Object
        res = (From x In modelo.Usuarios
               Where x.CodUsuario = resp
               Select x.CodUsuario, x.Nombre, x.Apellido, x.Direccion, x.Telefono).ToList()
        DataGridView2.DataSource = res
    End Sub

    'Instalaciones 1
    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click

        Dim resp As Date = CDate(InputBox("Mete fechaini"))
        Dim resp2 As Date = CDate(InputBox("Mete fecha fin"))
        Dim res As Object



        res = (From x In modelo.Instalaciones
               Where x.Fecha_Instalación >= resp And x.Fecha_Instalación <= resp2
               Select x.CodInstalacion, x.CodSoftware, x.Equipos, x.Fecha_Instalación, x.Software).ToList()



        DataGridView3.DataSource = res
    End Sub

    'Instalaciones 2
    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        Dim resp As String = InputBox("Mete Codigo de equipo")
        Dim res As Object



        res = (From x In modelo.Instalaciones
               Where x.Fecha_Instalación = resp
               Select New With {x.CodEquipo, x.CodInstalacion}).ToList()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Where x.CodEquipo = "0001" And x.Fecha_Instalación.Value.Year = 2007
               Select New With {x.CodEquipo, x.CodInstalacion, x.CodSoftware, x.Fecha_Instalación}).ToList()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Where x.CodSoftware = "0004" And x.Fecha_Instalación.Value.Year = 2004
               Select New With {x.CodEquipo, x.CodInstalacion, x.CodSoftware, x.Fecha_Instalación}).ToList()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Group By Software = x.CodSoftware
                       Into Instalaciones = Group
               Select New With {Software, Instalaciones.Count}).ToArray()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Group By Equipo = x.CodEquipo
                       Into Instalaciones = Group
               Select New With {Equipo, Instalaciones.Count}).ToArray()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Group By Software = x.CodSoftware
                       Into Instalaciones = Group
               Select New With {Software, .ultima = Instalaciones.Max(Function(x) x.Fecha_Instalación)}).ToArray()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Where x.CodEquipo = InputBox("Mete Codigo Equipo")
               Group By Equipo = x.CodEquipo
                       Into Instalaciones = Group
               Select New With {Equipo, .ultima = Instalaciones.Max(Function(x) x.Fecha_Instalación)}).ToArray()
        DataGridView3.DataSource = res
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        Dim res As Object

        res = (From x In modelo.Instalaciones
               Where x.Fecha_Instalación.Value.Year = DateTime.Now.Year
               Select New With {x.CodEquipo, x.CodInstalacion}).ToArray()
        DataGridView3.DataSource = res
    End Sub
End Class
