package com.example.galeria;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Base64;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtId, txtTitulo;
    ImageButton btnSearch;
    ImageView iv;
    Button btnSave;
    ImagenesDBOpenHelper idbh;
    boolean nueva=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iv = findViewById(R.id.img);
        iv.setOnClickListener(this);
        btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        txtId=findViewById(R.id.txtId);
        txtTitulo=findViewById(R.id.txtTitulo);
        btnSave=findViewById(R.id.button);
        btnSave.setOnClickListener(this);
        idbh= new ImagenesDBOpenHelper(this,"DBGaleria", null, 1);
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch){
            SQLiteDatabase db = idbh.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT id imagen FROM Imagenes WHERE id=?", new String[]{txtId.getText().toString()});
            if(c.moveToFirst()){
                txtTitulo.setText(c.getString(0));
                byte[] res=Base64.decode(c.getString(1),Base64.DEFAULT);
                Bitmap bmp = BitmapFactory.decodeByteArray(res,0,res.length);
                iv.setImageBitmap(bmp);
                nueva=false;
            }else{
                nueva=true;
            }

        }else if(v==btnSave){
            byte[] array;

            //AQUI

            BitmapDrawable bmp = (BitmapDrawable)iv.getDrawable();
            Base64.encodeToString(array,Base64.DEFAULT);


        }else if(v==iv){
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            i.setType(MediaStore.Images.Media.CONTENT_TYPE);
            startActivityForResult(i,770);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==770){
            if(resultCode==RESULT_OK){
                Uri uri=data.getData();
                try {
                    ContentResolver cr = getContentResolver();
                    Bitmap bmp = ImageDecoder.decodeBitmap(ImageDecoder.createSource(cr,uri));
                    iv.setImageBitmap(bmp);
                    Cursor c = cr.query(uri,
                            new String[]{MediaStore.Images.ImageColumns.DISPLAY_NAME},
                            null,
                            null,
                            null);
                    if(c.moveToFirst()){
                        String aux = c.getString(0);
                        String[] auxi=aux.split("/");
                        txtTitulo.setText(auxi[auxi.length-1]);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(this,"Mal hecho chaval",Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
